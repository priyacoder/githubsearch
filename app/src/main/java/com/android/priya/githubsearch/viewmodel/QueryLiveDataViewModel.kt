package com.android.priya.githubsearch.viewmodel

import android.arch.lifecycle.ViewModel
import android.content.SharedPreferences
import com.android.priya.githubsearch.service.repository.SearchQueryRepositoryImpl

class QueryLiveDataViewModel(sharedPreferences: SharedPreferences?, queryListKey : String) : ViewModel(){

    val searchQueryRepositoryImpl = SearchQueryRepositoryImpl(sharedPreferences, queryListKey)
}