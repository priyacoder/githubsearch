package com.android.priya.githubsearch

import android.app.Application
import android.content.Context
import com.apollographql.apollo.ApolloClient
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.Response
import okhttp3.logging.HttpLoggingInterceptor

class GithubSearchApplication : Application() {

    private val BASE_URL = "https://api.github.com/graphql"
    val apolloClient: ApolloClient by lazy { initClient() }
    val logging = HttpLoggingInterceptor()

    init {
        logging.level = HttpLoggingInterceptor.Level.BASIC
    }

    companion object {
        private lateinit var appContext : Context
    }

    override fun onCreate() {
        super.onCreate()
        appContext = this
    }

    private fun initClient(): ApolloClient {
        val okHttpClient = OkHttpClient.Builder()
                .addInterceptor(logging)
                .addNetworkInterceptor(NetworkInterceptor(appContext))
                .build()
        return ApolloClient.builder()
                .serverUrl(BASE_URL)
                .okHttpClient(okHttpClient)
                .build()
    }
    private class NetworkInterceptor(val context: Context): Interceptor {

        val accessTokenValue : String =
                context
                        .getSharedPreferences(context.packageName, Context.MODE_PRIVATE)
                        .getString(context.resources.getString(R.string.accessTokenKey),"")
        override fun intercept(chain: Interceptor.Chain?): Response {
            return chain!!.proceed(chain.request().newBuilder().header("Authorization", "Bearer $accessTokenValue").build())
        }
    }
}