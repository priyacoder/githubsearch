package com.android.priya.githubsearch.view.adapter

import android.databinding.DataBindingUtil
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import com.android.priya.githubsearch.R
import com.android.priya.githubsearch.databinding.GithubResultListItemBinding
import com.android.priya.githubsearch.service.model.GithubRepoInfo
import com.android.priya.githubsearch.view.callback.GithubRepoClickCallback

class GithubSearchAdapter(projectClickCallback: GithubRepoClickCallback?) : RecyclerView.Adapter<GithubSearchAdapter.GithubRepoViewHolder>() {

    private var reposList: List<GithubRepoInfo> = arrayListOf()
    private var repoClickCallback: GithubRepoClickCallback? = null

    init {
        this.repoClickCallback = projectClickCallback
    }

    override fun onBindViewHolder(holder: GithubRepoViewHolder, position: Int) {
        val repository = reposList[position]
        Log.d("GithubSearchAdapter", "Repository value $repository")
        holder.binding.githubRepo = repository
        holder.binding.executePendingBindings()
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): GithubRepoViewHolder {
        val binding: GithubResultListItemBinding = DataBindingUtil.inflate(LayoutInflater.from(parent.context), R.layout.github_result_list_item,
                parent, false)
        binding.callback = repoClickCallback

        return GithubRepoViewHolder(binding)
    }

    override fun getItemCount(): Int = reposList.size

    class GithubRepoViewHolder(var binding: GithubResultListItemBinding) : RecyclerView.ViewHolder(binding.root)

    fun setReposList(repositoriesList: List<GithubRepoInfo>) {
        val currentEndPos = this.reposList.size
        this.reposList = repositoriesList
        Log.d("GithubSearchAdapter", "Inside setReposList$repositoriesList")
        notifyItemRangeInserted(currentEndPos, reposList.size)
    }
}