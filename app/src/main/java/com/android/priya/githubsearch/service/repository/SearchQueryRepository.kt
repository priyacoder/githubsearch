package com.android.priya.githubsearch.service.repository

import android.arch.lifecycle.LiveData

interface SearchQueryRepository{

    fun addRecentQuery(query : String)
    fun getRecentQueries() : LiveData<List<String>>
}