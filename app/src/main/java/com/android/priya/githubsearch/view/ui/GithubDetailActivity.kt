package com.android.priya.githubsearch.view.ui

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.view.MenuItem
import com.android.priya.githubsearch.R
import com.android.priya.githubsearch.databinding.ActivityGithubDetailBinding
import com.android.priya.githubsearch.service.model.GithubRepoDetails
import com.android.priya.githubsearch.view.adapter.RepoSubscriberAdapter
import com.android.priya.githubsearch.viewmodel.GithubDetailViewModel

class GithubDetailActivity : AppCompatActivity(){

    val repoSubscriberAdapter : RepoSubscriberAdapter = RepoSubscriberAdapter()
    val activityGithubDetailBinding : ActivityGithubDetailBinding by lazy {
        DataBindingUtil.setContentView<ActivityGithubDetailBinding>(this,R.layout.activity_github_detail)
    }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val bundle = intent.extras
        val owner = bundle.getString("owner")
        val name = bundle.getString("repoName")

        activityGithubDetailBinding.subscribersList.layoutManager = LinearLayoutManager(this)
        activityGithubDetailBinding.subscribersList.adapter = repoSubscriberAdapter
        activityGithubDetailBinding.subscribersList.addItemDecoration(DividerItemDecoration(this,DividerItemDecoration.VERTICAL))
        val githubDetailViewModel = ViewModelProviders.of(this).get(GithubDetailViewModel::class.java)
        observeDetailViewModel(githubDetailViewModel, name, owner)

        //Add Back navigation to Search Results screen
        setBackNavigation()
    }

    private fun setBackNavigation() {
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        val itemId = item?.itemId
        if (itemId == android.R.id.home) {
            finish()
            onBackPressed()
            return true
        }
        return super.onOptionsItemSelected(item);
    }

    private fun observeDetailViewModel(viewModel: GithubDetailViewModel, repoName: String, owner: String) {
        val githubRepositoryDetails = viewModel.getProjectDetailsObservable(repoName, owner)
        githubRepositoryDetails
                .observe(this, object: Observer<GithubRepoDetails>{
                    override fun onChanged(repoDetails : GithubRepoDetails?) {
                        val count = repoDetails?.subscriberCount ?:0
                        activityGithubDetailBinding.isSubscribed = count > 0
                        activityGithubDetailBinding.githubRepoDetails = repoDetails
                        repoSubscriberAdapter.setSubscribers(repoDetails?.subscribers ?: emptyList())
                        repoSubscriberAdapter.notifyDataSetChanged()
                    }
                })
    }
}
