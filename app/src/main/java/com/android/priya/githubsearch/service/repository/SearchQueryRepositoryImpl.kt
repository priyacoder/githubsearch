package com.android.priya.githubsearch.service.repository

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.content.SharedPreferences
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken

class SearchQueryRepositoryImpl(private val sharedPreferences: SharedPreferences?, val queryListKey: String) : SearchQueryRepository, LiveData<ArrayList<String>>() {

    override fun getRecentQueries(): LiveData<List<String>> {
        return MutableLiveData<List<String>>()
                .also { it.postValue(getStoredSearches(queryListKey)) }
    }

    var editor: SharedPreferences.Editor? = null
    var gson: Gson = Gson()

    init {
        editor = sharedPreferences?.edit()
    }

    override fun addRecentQuery(query: String) {

        getStoredSearches(queryListKey)
                .takeLast(4)
                .let { gson.toJson(it + listOf(query)) }
                .let { editor?.putString(queryListKey, it)?.apply() }
        }

    private fun getStoredSearches(key: String): List<String> {
        val type = object : TypeToken<ArrayList<String>>() {}.type
        return sharedPreferences
                ?.getString(key, "[]")
                .let { gson.fromJson(it, type) }
    }
}