package com.android.priya.githubsearch.service.repository

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.util.Log
import com.apollographql.apollo.ApolloCall
import com.apollographql.apollo.api.Input
import com.apollographql.apollo.api.Response
import com.apollographql.apollo.exception.ApolloException
import com.android.priya.githubsearch.GithubSearchApplication
import com.android.priya.githubsearch.service.mappers.GithubRepoMapper
import com.android.priya.githubsearch.service.model.GithubRepoDetails
import com.android.priya.githubsearch.service.model.GithubRepoInfo
import com.codingtest.priya.githubsearch.FindGithubRepoDetailsQuery
import com.codingtest.priya.githubsearch.FindGithubReposQuery

typealias PagedGithubRepos = Pair<List<GithubRepoInfo>,Boolean>
class GithubProjectRepository {

    var mapper: GithubRepoMapper = GithubRepoMapper()

    fun fetchRepositories(
            githubQuery: String,
            data: MutableLiveData<PagedGithubRepos> = MutableLiveData(),
            after: String? = null,
            noOfRepos: Int
    ): MutableLiveData<PagedGithubRepos> {

        val apolloQueryCall = GithubSearchApplication().apolloClient.query(FindGithubReposQuery(githubQuery, Input.fromNullable(after), noOfRepos))

        apolloQueryCall.enqueue(object : ApolloCall.Callback<FindGithubReposQuery.Data>(){
            override fun onFailure(e: ApolloException) {
                Log.e("GithubProjectRepository","Something went wrong while fetching github repositories")
            }
            override fun onResponse(response: Response<FindGithubReposQuery.Data>) {
                val hasMorePages = response.data()?.search()?.pageInfo()?.hasNextPage() == true
                (response.data() as FindGithubReposQuery.Data)
                        .search()
                        .edges()
                        ?.map { mapper.graphQlGithubRepoToGithubRepo(it) }
                        ?.let { getAppendedResults(data, it, hasMorePages) }
                        ?.let { data.postValue(it) }
            }
        })
        return data
    }

    private fun getAppendedResults(data: MutableLiveData<PagedGithubRepos>, currentRepos: List<GithubRepoInfo>, hasMorePages: Boolean): PagedGithubRepos {
        val existingRepos = data.value?.first ?: emptyList()
        return PagedGithubRepos(existingRepos+currentRepos, hasMorePages)
    }

    fun fetchRepoDetails(name: String, owner: String): LiveData<GithubRepoDetails> {
        val data = MutableLiveData<GithubRepoDetails>()
        val apolloQueryCall = GithubSearchApplication().apolloClient.query(FindGithubRepoDetailsQuery(name, owner))

        apolloQueryCall.enqueue(object: ApolloCall.Callback<FindGithubRepoDetailsQuery.Data>(){
            override fun onFailure(e: ApolloException) {
                Log.e("GithubProjectRepository","Something went wrong while fetching github repository details")
            }

            override fun onResponse(response: Response<FindGithubRepoDetailsQuery.Data>) {
                val repository = response.data()?.repository() ?: throw NoRepositoryFoundException("Could not find repository with owner $owner and name $name")
                repository.let { mapper.graphQlGithubRepoDetailsToGithubRepoDetails(it) }
                        .let { data.postValue(it) }
            }
        })
        return data
    }
}