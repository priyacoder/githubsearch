package com.android.priya.githubsearch.service.model

data class GithubRepoInfo(val id : String,
                      val avatarImageUrl : String,
                      val name : String,
                      val owner: String,
                      val description: String,
                      val forks_count: String,
                      val cursor: String?)

data class GithubRepoDetails(val name: String,
                             val subscriberCount: Int,
                             val subscribers: List<String>)
