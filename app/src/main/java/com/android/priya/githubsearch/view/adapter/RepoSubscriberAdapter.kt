package com.android.priya.githubsearch.view.adapter

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.android.priya.githubsearch.R
import kotlinx.android.synthetic.main.subscribers_list_item.view.*

class RepoSubscriberAdapter : RecyclerView.Adapter<RepoSubscriberAdapter.RepoSubscriberViewHolder>(){
    private var subscribersList : List<String> = arrayListOf()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RepoSubscriberAdapter.RepoSubscriberViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val view = inflater.inflate(R.layout.subscribers_list_item, parent, false)
        return RepoSubscriberViewHolder(view)
    }

    override fun getItemCount(): Int {
        return subscribersList.size
    }

    override fun onBindViewHolder(holder: RepoSubscriberAdapter.RepoSubscriberViewHolder, position: Int) {
        val name = subscribersList[position]
        holder.subscriberName.text = name
    }

    class RepoSubscriberViewHolder(v: View) : RecyclerView.ViewHolder(v){
        val subscriberName = v.subscriberName
    }

    fun setSubscribers(subscribers : List<String>){
        subscribersList = subscribers
    }
}