package com.android.priya.githubsearch.view.ui

import android.app.Activity
import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProvider
import android.arch.lifecycle.ViewModelProviders
import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.os.PersistableBundle
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.view.ViewGroup
import android.widget.SearchView
import android.widget.TextView
import com.android.priya.githubsearch.R
import com.android.priya.githubsearch.viewmodel.QueryLiveDataViewModel
import kotlinx.android.synthetic.main.activity_github_search.*
import kotlinx.android.synthetic.main.github_access.*
import kotlinx.android.synthetic.main.github_repo_search.*

class GithubSearchActivity : AppCompatActivity(), View.OnClickListener {
    override fun onClick(v: View?) {
        val query = (v as? TextView)?.text.toString()
        startResultActivity(query)
    }

    private lateinit var viewModel: QueryLiveDataViewModel
    private lateinit var sharedPref: SharedPreferences

    override fun onCreate(savedInstanceState: Bundle?) {

        val resources = applicationContext.resources
        val queryKey = resources.getString(R.string.prefKey)
        val accessTokenKey = resources.getString(R.string.accessTokenKey)
        sharedPref = applicationContext.getSharedPreferences(applicationContext.packageName, Activity.MODE_PRIVATE)
        viewModel = ViewModelProviders.of(this@GithubSearchActivity, object : ViewModelProvider.Factory {
            override fun <QueryLiveDataViewModel : ViewModel?> create(modelClass: Class<QueryLiveDataViewModel>): QueryLiveDataViewModel {
                return (QueryLiveDataViewModel(sharedPreferences = sharedPref, queryListKey = queryKey)) as QueryLiveDataViewModel
            }
        }).get(QueryLiveDataViewModel::class.java)
        val accessToken = sharedPref.getString(accessTokenKey, "")
        val mockAccessTokenValue = resources.getString(R.string.mockAccessTokenValue)

        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_github_search)

        //Verify if access Token is already entered and set layout accordingly
        if (accessToken.isEmpty()) {
            githubRepoSearchLayout.visibility = View.GONE
            githubAccessLayout.visibility = View.VISIBLE

            //Comment this part in production
            enterAccessToken.setText(mockAccessTokenValue)

            //Save Token value and refresh page on click of save
            saveToken.setOnClickListener { saveAccessToken(accessTokenKey) }
        } else {
            githubRepoSearchLayout.visibility = View.VISIBLE
            githubAccessLayout.visibility = View.GONE

            //Load latest queries in the flow layout
            displayRecentSearches()

            //To show query entered during configuration changes like rotation
            if (savedInstanceState != null) {
                val queryValue = savedInstanceState.getString(applicationContext.packageName)
                et_searchBox.setQuery(queryValue, false)
            }

            // Open Results View on submitting the query
            showResultsOnSubmit()
        }
    }

    private fun saveAccessToken(accessTokenKey: String?) {
        sharedPref.edit().putString(accessTokenKey,enterAccessToken.text.toString()).apply()
        val searchRepoIntent = Intent(this@GithubSearchActivity, GithubSearchActivity::class.java)
        startActivity(searchRepoIntent)
        finish()
    }

    private fun showResultsOnSubmit() {
        et_searchBox.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextChange(newText: String?): Boolean {
                return false
            }

            override fun onQueryTextSubmit(query: String): Boolean {
                query
                        .also { viewModel.searchQueryRepositoryImpl.addRecentQuery(query) }
                        .let { startResultActivity(it) }
                return true
            }
        })
    }

    private fun startResultActivity(query: String) {
        val githubRepoIntent = Intent(this@GithubSearchActivity, GithubResultActivity::class.java)

        with(githubRepoIntent) {
            putExtra(resources.getString(R.string.query_entered), query)
            flags = Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_NEW_TASK
        }
        startActivity(githubRepoIntent)
        finish()
    }

    private fun displayRecentSearches() {

        viewModel.searchQueryRepositoryImpl.getRecentQueries().observe(this@GithubSearchActivity, Observer<List<String>> {
            updateFlowLayout(it ?: listOf())
        })
    }

    private fun updateFlowLayout(queryList: List<String>) {
        val layoutParams = ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT)
        val paddingSmall = resources.getDimension(R.dimen.padding_small).toInt()
        queryList
                .forEach {
                    val textView = TextView(this)
                    textView.layoutParams = layoutParams
                    textView.background = getDrawable(R.drawable.rounded_corners)
                    textView.setPadding(paddingSmall, paddingSmall, paddingSmall, paddingSmall)
                    textView.text = it
                    textView.setOnClickListener(this)
                    fl_recentSearches.addView(textView)
                }
    }

    override fun onSaveInstanceState(outState: Bundle?, outPersistentState: PersistableBundle?) {
        outState?.putString(applicationContext.packageName, et_searchBox.query.toString())
        super.onSaveInstanceState(outState, outPersistentState)
    }
}
