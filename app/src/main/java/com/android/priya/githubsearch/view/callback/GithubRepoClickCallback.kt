package com.android.priya.githubsearch.view.callback

import com.android.priya.githubsearch.service.model.GithubRepoInfo

interface GithubRepoClickCallback{
    fun onClick(githubRepo: GithubRepoInfo)
}