package com.android.priya.githubsearch.viewmodel

import android.app.Application
import android.arch.lifecycle.AndroidViewModel
import android.arch.lifecycle.MutableLiveData
import com.android.priya.githubsearch.service.repository.GithubProjectRepository
import com.android.priya.githubsearch.service.repository.PagedGithubRepos

class GithubSearchViewModel(application: Application) : AndroidViewModel(application) {

    fun loadGithubRepos(query: String, data: MutableLiveData<PagedGithubRepos>, after: String?, noOfRepos: Int) : MutableLiveData<PagedGithubRepos>{
        return GithubProjectRepository().fetchRepositories(query, data = data, after = after, noOfRepos = noOfRepos)
    }
}