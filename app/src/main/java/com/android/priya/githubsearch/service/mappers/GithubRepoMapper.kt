package com.android.priya.githubsearch.service.mappers

import com.android.priya.githubsearch.service.model.GithubRepoDetails
import com.android.priya.githubsearch.service.model.GithubRepoInfo
import com.codingtest.priya.githubsearch.FindGithubRepoDetailsQuery
import com.codingtest.priya.githubsearch.FindGithubReposQuery

class GithubRepoMapper {
    fun graphQlGithubRepoToGithubRepo(edge: FindGithubReposQuery.Edge?): GithubRepoInfo{
        val cursor = edge?.cursor()
        val repo = edge?.node() as FindGithubReposQuery.AsRepository
            return GithubRepoInfo(
                    id = repo.id(),
                    name = repo.name(),
                    owner = repo.owner().login(),
                    description = repo.description() ?: "",
                    avatarImageUrl = repo.owner().avatarUrl(),
                    forks_count = repo.forkCount().toString(),
                    cursor = cursor
            )
    }

    fun graphQlGithubRepoDetailsToGithubRepoDetails(repoDetail: FindGithubRepoDetailsQuery.Repository): GithubRepoDetails{
        return GithubRepoDetails(
                name = repoDetail.name(),
                subscriberCount = repoDetail.watchers().totalCount(),
                subscribers = repoDetail.watchers().nodes()?.map { it.login() } ?: emptyList()
        )
    }
}