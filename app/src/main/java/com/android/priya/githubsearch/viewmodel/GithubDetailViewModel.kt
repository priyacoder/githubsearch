package com.android.priya.githubsearch.viewmodel

import android.app.Application
import android.arch.lifecycle.AndroidViewModel
import android.arch.lifecycle.LiveData
import com.android.priya.githubsearch.service.model.GithubRepoDetails
import com.android.priya.githubsearch.service.repository.GithubProjectRepository

class GithubDetailViewModel(application : Application) : AndroidViewModel(application) {
        fun getProjectDetailsObservable(name: String, owner: String) : LiveData<GithubRepoDetails> {
            return GithubProjectRepository().fetchRepoDetails(name, owner)
        }
}