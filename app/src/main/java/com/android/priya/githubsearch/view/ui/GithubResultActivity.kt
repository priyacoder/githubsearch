package com.android.priya.githubsearch.view.ui

import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.content.Intent
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.MenuItem
import com.android.priya.githubsearch.R
import com.android.priya.githubsearch.databinding.ActivityGithubResultBinding
import com.android.priya.githubsearch.service.model.GithubRepoInfo
import com.android.priya.githubsearch.service.repository.PagedGithubRepos
import com.android.priya.githubsearch.view.adapter.GithubSearchAdapter
import com.android.priya.githubsearch.view.callback.GithubRepoClickCallback
import com.android.priya.githubsearch.viewmodel.GithubSearchViewModel
import kotlinx.android.synthetic.main.activity_github_result.*

class GithubResultActivity : AppCompatActivity() {
    val PAGE_SIZE = 5
    var githubSearchAdapter: GithubSearchAdapter? = null
    var isLastPage = false
    var githubReposSource: MutableLiveData<PagedGithubRepos> = MutableLiveData<PagedGithubRepos>()
    val activityGithubResultBinding : ActivityGithubResultBinding by lazy {
        DataBindingUtil.setContentView<ActivityGithubResultBinding>(this@GithubResultActivity,R.layout.activity_github_result)
    }
    private val linearLayoutManager : LinearLayoutManager by lazy {
        LinearLayoutManager(this@GithubResultActivity)
    }
    private val viewModel by lazy {
        ViewModelProviders.of(this).get(GithubSearchViewModel::class.java)
    }

    private fun getQuery(): String {
        return intent.getStringExtra(this.resources.getString(R.string.query_entered))
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        githubSearchAdapter = GithubSearchAdapter(githubRepoClickCallback)

        activityGithubResultBinding.repoList.adapter = githubSearchAdapter
        activityGithubResultBinding.repoList.layoutManager = linearLayoutManager
        activityGithubResultBinding.repoList.addOnScrollListener(recyclerViewOnScrollListener)
        activityGithubResultBinding.isLoading = true
        activityGithubResultBinding.hasSearchResults = true
        progressBar.bringToFront()

        observeViewModel()
        loadMoreRepositories(initial = true)

        //Add Back navigation
        setBackNavigation()
    }

    fun setBackNavigation() {
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
    }


    private val githubRepoClickCallback = object : GithubRepoClickCallback {
        override fun onClick(githubRepo: GithubRepoInfo) {
            startDetailActivity(githubRepo)
        }
    }

    private fun startDetailActivity(githubRepo: GithubRepoInfo) {
        val githubDetailIntent = Intent(this, GithubDetailActivity::class.java)
        githubDetailIntent.flags = Intent.FLAG_ACTIVITY_SINGLE_TOP
        githubDetailIntent.putExtra("owner", githubRepo.owner)
        githubDetailIntent.putExtra("repoName", githubRepo.name)
        startActivity(githubDetailIntent)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        val searchActivityIntent = Intent(this, GithubSearchActivity::class.java)
        searchActivityIntent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_NEW_TASK
        startActivityForResult(searchActivityIntent,0)
        finish()
        return true
    }

    private fun observeViewModel() {
        githubReposSource.observe(this, object : Observer<PagedGithubRepos> {
            override fun onChanged(searchResult: PagedGithubRepos?) {

                val listOfGithubRepos = searchResult?.first
                val hasNextPage = searchResult?.second

                with(activityGithubResultBinding) {
                    hasSearchResults = getSize(listOfGithubRepos) > 0
                    isLoading = false
                }
                isLastPage = !(hasNextPage ?: true)
                listOfGithubRepos?.let { githubSearchAdapter?.setReposList(it) }
            }
        })
    }

    private fun getSize(githubRepos: List<GithubRepoInfo>?) = githubRepos?.size ?: 0

    private val recyclerViewOnScrollListener : RecyclerView.OnScrollListener =
            object : RecyclerView.OnScrollListener(){
                override fun onScrollStateChanged(recyclerView: RecyclerView?, newState: Int) {
                    super.onScrollStateChanged(recyclerView, newState)
                }

                override fun onScrolled(recyclerView: RecyclerView?, dx: Int, dy: Int) {
                    super.onScrolled(recyclerView, dx, dy)
                    val visibleItemCount = linearLayoutManager.childCount
                    val totalItemCount = linearLayoutManager.itemCount
                    val firstVisibleItemPosition = linearLayoutManager.findFirstVisibleItemPosition()

                    if (activityGithubResultBinding.isLoading != true && !isLastPage) {
                        if ((visibleItemCount + firstVisibleItemPosition) >= totalItemCount
                                && firstVisibleItemPosition >= 0
                                && totalItemCount >= PAGE_SIZE) {
                            loadMoreRepositories(initial = false)
                        }
                    }
                }
            }

    private fun loadMoreRepositories(initial: Boolean) {
        activityGithubResultBinding.isLoading = true
        val after = if(initial) null else (githubReposSource.value as PagedGithubRepos).first.last().cursor
        githubReposSource = viewModel.loadGithubRepos(getQuery(), githubReposSource, after, PAGE_SIZE)

    }
}