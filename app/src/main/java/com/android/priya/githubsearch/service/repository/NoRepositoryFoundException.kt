package com.android.priya.githubsearch.service.repository

import java.lang.Exception

class NoRepositoryFoundException(errorMessage: String) : Exception(errorMessage){

}